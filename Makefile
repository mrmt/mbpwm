# $Id: Makefile,v 1.1 1999-01-14 07:41:31 morimoto Exp $

HTML		=	g.html
HTMLS		=	g-1.html g-2.html g-3.html g-4.html g-5.html g-6.html \
				g-7.html g-8.html g-9.html g-10.html g-11.html g-12.html \
				g-13.html g-14.html g-15.html
INFO		=	g.info
TARGET		=	$(HTML) $(INFO)
SGML_SRC	=	g.sgml
INFO_SRC	=	g.texi
EMACS		=	mule

all: $(TARGET)

clean:
	rm -f $(HTMLS)
$(HTML): $(SGML_SRC)
	sgml2html -l ja -c nippon $<
$(INFO): $(INFO_SRC)
	$(EMACS) -batch -funcall batch-texinfo-format $<

# EOF
